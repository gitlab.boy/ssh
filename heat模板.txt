cat server.yaml 

heat_template_version: 2014-10-16 
description: Generated template 
resources: 
  nova_flavor: 
    type: OS::Nova::Flavor 
    properties: 
      name: m1.flavor 
      disk: 20 
      is_public: True 
      ram: 1024 
      vcpus: 2 
      flavorid: 1234